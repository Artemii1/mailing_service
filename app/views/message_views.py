from flask import Blueprint, jsonify, request
from app.models import Message
from app import db

message_bp = Blueprint('message', __name__)

# Add new message
@message_bp.route('/messages', methods=['POST'])
def add_message():
    data = request.json
    new_message = Message(client_id=data['client_id'], mailing_id=data['mailing_id'], status=data['status'])
    db.session.add(new_message)
    db.session.commit()
    return jsonify({"code": 200, "message": "Сообщение успешно добавлено"}), 200

# Update message
@message_bp.route('/messages/<int:message_id>', methods=['PUT'])
def update_message(message_id):
    message = Message.query.get(message_id)
    if not message:
        return jsonify({"code": 400, "message": "Сообщение не найдено"}), 400
    data = request.json
    message.client_id = data['client_id']
    message.mailing_id = data['mailing_id']
    message.status = data['status']
    db.session.commit()
    return jsonify({"code": 200, "message": "Сообщение успешно обновлено"}), 200

# Delete message
@message_bp.route('/messages/<int:message_id>', methods=['DELETE'])
def delete_message(message_id):
    message = Message.query.get(message_id)
    if not message:
        return jsonify({"code": 400, "message": "Сообщение не найдено"}), 400
    db.session.delete(message)
    db.session.commit()
    return jsonify({"code": 200, "message": "Сообщение успешно удалено"}), 200

# Get all messages
@message_bp.route('/messages', methods=['GET'])
def get_messages():
    messages = Message.query.all()
    return jsonify([message.to_dict() for message in messages]), 200

# Get a single message
@message_bp.route('/messages/<int:message_id>', methods=['GET'])
def get_message(message_id):
    message = Message.query.get(message_id)
    if not message:
        return jsonify({"code": 400, "message": "Сообщение не найдено"}), 400
    return jsonify(message.to_dict()), 200
