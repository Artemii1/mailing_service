from flask import Blueprint, jsonify, request
from app.models import Mailing
from app import db

mailing_bp = Blueprint('mailing', __name__)

# Add new mailing
@mailing_bp.route('/mailings', methods=['POST'])
def add_mailing():
    data = request.json
    new_mailing = Mailing(start_time=data['start_time'], end_time=data['end_time'], message_text=data['message_text'], filter_properties=data['filter_properties'])
    db.session.add(new_mailing)
    db.session.commit()
    return jsonify({"code": 200, "message": "Рассылка успешно добавлена"}), 200

# Update mailing
@mailing_bp.route('/mailings/<int:mailing_id>', methods=['PUT'])
def update_mailing(mailing_id):
    mailing = Mailing.query.get(mailing_id)
    if not mailing:
        return jsonify({"code": 400, "message": "Рассылка не найдена"}), 400
    data = request.json
    mailing.start_time = data['start_time']
    mailing.end_time = data['end_time']
    mailing.message_text = data['message_text']
    mailing.filter_properties = data['filter_properties']
    db.session.commit()
    return jsonify({"code": 200, "message": "Рассылка успешно обновлена"}), 200

# Delete mailing
@mailing_bp.route('/mailings/<int:mailing_id>', methods=['DELETE'])
def delete_mailing(mailing_id):
    mailing = Mailing.query.get(mailing_id)
    if not mailing:
        return jsonify({"code": 400, "message": "Рассылка не найдена"}), 400
    db.session.delete(mailing)
    db.session.commit()
    return jsonify({"code": 200, "message": "Рассылка успешно удалена"}), 200

# Get all mailings
@mailing_bp.route('/mailings', methods=['GET'])
def get_mailings():
    mailings = Mailing.query.all()
    return jsonify([mailing.to_dict() for mailing in mailings]), 200

# Get a single mailing
@mailing_bp.route('/mailings/<int:mailing_id>', methods=['GET'])
def get_mailing(mailing_id):
    mailing = Mailing.query.get(mailing_id)
    if not mailing:
        return jsonify({"code": 400, "message": "Рассылка не найдена"}), 400
    return jsonify(mailing.to_dict()), 200
