from flask import Flask

def register_blueprints(app: Flask):
    from .client_views import client_bp
    from .mailing_views import mailing_bp
    from .message_views import message_bp

    app.register_blueprint(client_bp, url_prefix='/api/clients')
    app.register_blueprint(mailing_bp, url_prefix='/api/mailings')
    app.register_blueprint(message_bp, url_prefix='/api/messages')

