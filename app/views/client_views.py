from flask import Blueprint, jsonify, request
from app.models import Client
from app import db

client_bp = Blueprint('client', __name__)

# Add new client
@client_bp.route('/clients', methods=['POST'])
def add_client():
    data = request.json
    new_client = Client(phone_number=data['phone_number'], operator_code=data['operator_code'], tag=data['tag'], timezone=data['timezone'])
    db.session.add(new_client)
    db.session.commit()
    return jsonify({"code": 200, "message": "Клиент успешно добавлен"}), 200

# Update client
@client_bp.route('/clients/<int:client_id>', methods=['PUT'])
def update_client(client_id):
    client = Client.query.get(client_id)
    if not client:
        return jsonify({"code": 400, "message": "Клиент не найден"}), 400
    data = request.json
    client.phone_number = data['phone_number']
    client.operator_code = data['operator_code']
    client.tag = data['tag']
    client.timezone = data['timezone']
    db.session.commit()
    return jsonify({"code": 200, "message": "Клиент успешно обновлен"}), 200

# Delete client
@client_bp.route('/clients/<int:client_id>', methods=['DELETE'])
def delete_client(client_id):
    client = Client.query.get(client_id)
    if not client:
        return jsonify({"code": 400, "message": "Клиент не найден"}), 400
    db.session.delete(client)
    db.session.commit()
    return jsonify({"code": 200, "message": "Клиент успешно удален"}), 200

# Get all clients
@client_bp.route('/clients', methods=['GET'])
def get_clients():
    clients = Client.query.all()
    return jsonify([client.to_dict() for client in clients]), 200

# Get a single client
@client_bp.route('/clients/<int:client_id>', methods=['GET'])
def get_client(client_id):
    client = Client.query.get(client_id)
    if not client:
        return jsonify({"code": 400, "message": "Клиент не найден"}), 400
    return jsonify(client.to_dict()), 200
