from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from config import Config
from app.models import db
from app.views import register_blueprints

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)

    with app.app_context():
        db.create_all()
        register_blueprints(app)

    return app
