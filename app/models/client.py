from datetime import datetime
from app import db

class Client(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    phone_number = db.Column(db.String(12), unique=True, nullable=False)
    operator_code = db.Column(db.String(10), nullable=False)
    tag = db.Column(db.String(50), nullable=True)
    timezone = db.Column(db.String(50), nullable=False)

    def __repr__(self):
        return f"Client('{self.phone_number}', '{self.operator_code}', '{self.tag}', '{self.timezone}')"
