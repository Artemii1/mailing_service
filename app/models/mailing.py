from datetime import datetime
from app import db

class Mailing(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    start_time = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    end_time = db.Column(db.DateTime, nullable=False)
    message_text = db.Column(db.Text, nullable=False)
    filter_properties = db.Column(db.String(100), nullable=False)

    messages = db.relationship('Message', backref='mailing', lazy=True)

    def __repr__(self):
        return f"Mailing('{self.start_time}', '{self.message_text}', '{self.filter_properties}')"
