from datetime import datetime
from app import db

class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    creation_time = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    status = db.Column(db.String(20), nullable=False, default="Pending")
    client_id = db.Column(db.Integer, db.ForeignKey('client.id'), nullable=False)
    mailing_id = db.Column(db.Integer, db.ForeignKey('mailing.id'), nullable=False)

    client = db.relationship('Client', backref=db.backref('messages', lazy=True))

    def __repr__(self):
        return f"Message('{self.creation_time}', '{self.status}', 'Client: {self.client_id}', 'Mailing: {self.mailing_id}')"
