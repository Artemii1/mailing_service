# Сервис Управления Рассылками

Сервис предоставляет возможность управления рассылками API администрирования и получения статистики. Пользователи могут создавать новые рассылки, просматривать созданные рассылки и получать статистику по выполненным рассылкам.

## Установка и настройка

### Предварительные требования

- Python 3.x
- pip

1. **Клонировать репозиторий**: 
    ```bash
    git clone https://gitlab.com/first1781237/mailing_service.git
    ```

2. **Настройка виртуальной среды** (необязательно, но рекомендуется):
    ```bash
    python3 -m venv venv
    source venv/bin/activate  # На Windows используйте `venv\Scripts\activate`
    ```

3. **Установка зависимостей**:
    ```bash
    pip install -r requirements.txt
    ```

4. **Запуск приложения**:
    ```bash
    python run.py
    ```

5. **Документация OpenAPI**:
    ```
    /docs/openapi.yaml
    ```

## Документация по API для интеграции

### Клиент

1. **Добавление нового клиента**

   - **Endpoint**: `/client`
   - **Метод**: `POST`
   - **Тело запроса**:
     ```json
     {
       "phone": "79123456789",
       "operator_code": "912",
       "tag": "sample_tag",
       "timezone": "UTC+3"
     }
     ```
   - **Ответ**:
     ```json
     {
       "id": 1,
       "phone": "79123456789"
     }
     ```

2. **Обновление данных клиента**

   - **Endpoint**: `/client/<id>`
   - **Метод**: `PUT`
   - **Тело запроса**:
     ```json
     {
       "phone": "79123456789",
       "operator_code": "912",
       "tag": "sample_tag",
       "timezone": "UTC+3"
     }
     ```
   - **Ответ**:
     ```json
     {
       "id": 1,
       "phone": "79123456789"
     }
     ```

3. **Удаление клиента**

   - **Endpoint**: `/client/<id>`
   - **Метод**: `DELETE`
   - **Тело запроса**:
     ```json
     {
       "phone": "79123456789",
       "operator_code": "912",
       "tag": "sample_tag",
       "timezone": "UTC+3"
     }
     ```
   - **Ответ**:
     ```json
     {
       "id": 1,
       "phone": "79123456789"
     }
     ```


### Рассылка

1. **Добавление новой рассылки**

   - **Endpoint**: `/mailing`
   - **Метод**: `POST`
   - **Тело запроса**:
     ```json
     {
       "start_time": "2023-09-17T15:00:00Z",
       "end_time": "2023-09-17T16:00:00Z",
       "message_text": "Текст вашего сообщения здесь",
       "client_filter": {
           "operator_code": "912",
           "tag": "sample_tag"
       }
     }
     ```
   - **Ответ**:
     ```json
     {
       "id": 1,
       "status": "scheduled"
     }
     ```

2. **Обновление данных рассылки**

   - **Endpoint**: `/mailing/<id>`
   - **Метод**: `PUT`
   - **Тело запроса**:
     ```json
     {
       "start_time": "2023-09-18T15:00:00Z",
       "end_time": "2023-09-18T16:00:00Z",
       "message_text": "Обновленный текст вашего сообщения",
     }
     ```
   - **Ответ**:
     ```json
     {
       "id": 1,
       "status": "updated"
     }
     ```

3. **Удаление рассылки**

   - **Endpoint**: `/mailing/<id>`
   - **Метод**: `DELETE`
   - **Ответ**:
     ```json
     {
       "id": 1,
       "status": "deleted"
     }
     ```

### Сообщение

1. **Получение статистики по сообщениям рассылки**

   - **Endpoint**: `/messages/<mailing_id>`
   - **Метод**: `GET`
   - **Ответ**:
     ```json
     {
       "mailing_id": 1,
       "total_sent": 100,
       "total_failed": 5
     }
     ```

2. **Обновление статуса сообщения**

   - **Endpoint**: `/message/<id>`
   - **Метод**: `PUT`
   - **Тело запроса**:
     ```json
     {
       "status": "sent"
     }
     ```
   - **Ответ**:
     ```json
     {
       "id": 1,
       "status": "sent"
     }
     ```

3. **Удаление сообщения**

   - **Endpoint**: `/message/<id>`
   - **Метод**: `DELETE`
   - **Ответ**:
     ```json
     {
       "id": 1,
       "status": "deleted"
     }
     ```



